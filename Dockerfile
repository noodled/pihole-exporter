FROM python:alpine

ENV PIHOST "localhost:80"
ENV INTERFACE "0.0.0.0"
ENV PORT 9311
ENV APIKEY ""

WORKDIR /usr/src/app
COPY . ./
RUN pip install --no-cache-dir -r requirements.txt

CMD python ./pihole-exporter/pihole-exporter.py --pihole $PIHOST --interface 0.0.0.0 --port $PORT --auth $APIKEY