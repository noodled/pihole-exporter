from setuptools import setup, find_packages

setup(
    name='pihole-exporter',
    version='0.1.dev0',
    url='https://gitlab.com/onedr0p/pihole-exporter',
    author='onedr0p',
    license='MIT',
    description='Export pihole metrics for prometheus',
    install_requires=["Flask"],
    packages=find_packages(),
    include_package_data = True,
    entry_points={'console_scripts': ['pihole-exporter=pihole-exporter.pihole-exporter:main']},
)
